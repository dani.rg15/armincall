# ArminCall

It's an application for call centers. Provides several CRM's features to handle common customer needs. It also allows to integrate with third-party communications company to recieve incomming phone calls in real time.

#### Backend
Built with Node.js, ExpressJs, and MongoDB

#### Backend
Built with Angular4 and Boostrap.

### Use

```
npm install 

cp .env.example .env

npm start
```